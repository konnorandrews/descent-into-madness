---
title: "Why?"
---

Why not? This site is a catalog of the weird things I have done and found out about [Rust](https://www.rust-lang.org/). Maybe these things will be useful to others. I don't know. All I know is that the decent never ends; The cosmos is infinite.
