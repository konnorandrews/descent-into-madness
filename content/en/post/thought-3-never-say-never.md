---
date: 2024-05-29T10:54:00-07:00
tags: ["never"]
title: "Thought 3: Never Say Never"
---

Have you ever seen the `!` type? Maybe as the return type of a function `fn() -> !`. Do you know what it means?

<!--more-->

`!` is called the never type. It represents a value that never exists. You may think this is a useless type then. "Why would we need a type that never exists as a value!?" Well what if I told you we can make one from scratch.

We just need to define a very strange enum.
```rust
enum Never {}
```
There we go. This type doesn't have any values because it has no variants. Fun right?

So why is `!` a thing? The answer is it has some nice properties because rustc knows about it. (see [here](https://github.com/rust-lang/rfcs/blob/master/text/1216-bang-type.md) for details)

So... how do we use `!` if its still nightly? Well turns out do to a bug in rustc `!` has been a normal type since version 1.12.0. All we need is some special magic to poof it into existence.
```rust
trait Extract {
    type T;
}

impl<T> Extract for fn() -> T {
    type T = T;
}

type Never = <fn() -> ! as Extract>::T;
```
Turns out `!` participates in generics so we can just use a blanket impl to extract it from a function pointer where we are allowed to write it.

How do we know this is the real `!` type? Well we can have rustc tell us it is.
```rust
trait Extract {
    type T;
}

impl<T> Extract for fn() -> T {
    type T = T;
}

type Never = <fn() -> ! as Extract>::T;

fn main() {
    let x: Never = 42i32;
}
```
```rust
error[E0308]: mismatched types
  --> src/main.rs:12:20
   |
12 |     let x: Never = 42i32;
   |            -----   ^^^^^ expected `!`, found `i32`
   |            |
   |            expected due to this
   |
   = note: expected type `!`
              found type `i32`
```
[Rust Explorer](https://www.rustexplorer.com/b#dHJhaXQgRXh0cmFjdCB7CiAgICB0eXBlIFQ7Cn0KCmltcGw8VD4gRXh0cmFjdCBmb3IgZm4oKSAtPiBUIHsKICAgIHR5cGUgVCA9IFQ7Cn0KCnR5cGUgTmV2ZXIgPSA8Zm4oKSAtPiAhIGFzIEV4dHJhY3Q+OjpUOwoKZm4gbWFpbigpIHsKICAgIGxldCB4OiBOZXZlciA9IDQyaTMyOwp9)

See its the real `!`. Who cares that its a nightly feature, we want to use `!` now! not never. Never say never to never.

---

Shout out to Yandros' [never_say_never](https://docs.rs/never-say-never/latest/never_say_never/) crate.
