---
date: 2024-05-23T11:55:00-07:00
tags: ["fish", "syntax"]
title: "Thought 1: A Fish"
---

You know the [turbofish](https://turbo.fish/)? This little guy `::<>`. Well turns out they live between all items. They apparently just hide most of the time.

<!--more-->

```rs
mod my_module {
    pub struct MyStruct;

    impl MyStruct {
        pub fn my_function() {}
    }
}

fn main() {
    my_module::<>::MyStruct::<>::my_function::<>();
}
```
[Rust Explorer](https://www.rustexplorer.com/b#bW9kIG15X21vZHVsZSB7CiAgICBwdWIgc3RydWN0IE15U3RydWN0OwoKICAgIGltcGwgTXlTdHJ1Y3QgewogICAgICAgIHB1YiBmbiBteV9mdW5jdGlvbigpIHt9CiAgICB9Cn0KCmZuIG1haW4oKSB7CiAgICBteV9tb2R1bGU6Ojw+OjpNeVN0cnVjdDo6PD46Om15X2Z1bmN0aW9uOjo8PigpOwp9)
