---
date: 2024-06-01T05:21:00-07:00
tags: ["where", "bound"]
title: "Thought 4: Alternative Universes"
---

Have you ever seen `where Self: Sized` on a trait's function? What does that mean? And how can we use this feature to make `String` implement `Copy`?

<!--more-->

As an example of this in the wild see [`Read::by_ref`](https://doc.rust-lang.org/std/io/trait.Read.html#method.by_ref). `Read` is ment to be object safe so you can do `&mut dyn Read`, but how is that possible if one of it's methods returns a `&mut Self`?

```rust
trait Read {
    fn by_ref(&mut self) -> &mut Self;
}

fn demo(x: &mut dyn Read) {}

fn main() {}
```
```
error[E0038]: the trait `Read` cannot be made into an object
 --> src/main.rs:5:17
  |
5 | fn demo(x: &mut dyn Read) {
  |                 ^^^^^^^^ `Read` cannot be made into an object
  |
note: for a trait to be "object safe" it needs to allow building a vtable to allow the call to be resolvable dynamically; for more information visit <https://doc.rust-lang.org/reference/items/traits.html#object-safety>
 --> src/main.rs:2:29
  |
1 | trait Read {
  |       ---- this trait cannot be made into an object...
2 |     fn by_ref(&mut self) -> &mut Self;
  |                             ^^^^^^^^^ ...because method `by_ref` references the `Self` type in its return type
  = help: consider moving `by_ref` to another trait
```
[Rust Explorer](https://www.rustexplorer.com/b#dHJhaXQgUmVhZCB7CiAgICBmbiBieV9yZWYoJm11dCBzZWxmKSAtPiAmbXV0IFNlbGY7Cn0KCmZuIGRlbW8oeDogJm11dCBkeW4gUmVhZCkgewogICAgCn0KCmZuIG1haW4oKSB7CiAgICAKfQ==)

See it can't be a trait object! But ... what if we add a bound that `Self` needs to *not* a trait object? But how to do that? Well trait objects are [dynamically sized types](https://doc.rust-lang.org/nomicon/exotic-sizes.html#dynamically-sized-types-dsts) so they don't implement `Sized`. So lets force `Self` to implement `Sized` and see what rustc says.

```rust
trait Read {
    fn by_ref(&mut self) -> &mut Self
    where
        Self: Sized;
}

fn demo(x: &mut dyn Read) {}
```
```
Finished `dev` profile [unoptimized + debuginfo] target(s) in 0.06s
```
[Rust Explorer](https://www.rustexplorer.com/b#dHJhaXQgUmVhZCB7CiAgICBmbiBieV9yZWYoJm11dCBzZWxmKSAtPiAmbXV0IFNlbGYKICAgIHdoZXJlCiAgICAgICAgU2VsZjogU2l6ZWQ7Cn0KCmZuIGRlbW8oeDogJm11dCBkeW4gUmVhZCkgewogICAgCn0KCmZuIG1haW4oKSB7CiAgICAKfQ==)

It works! When its a trait object the `by_ref` method doesn't exist so its allowed to be a trait object.

This opens a question. What happens if we do a blanket impl where the `Self` type is allowed to not implement `Sized`?

```rust
trait Read {
    fn by_ref(&mut self) -> &mut Self
    where
        Self: Sized;
}

impl<T: ?Sized> Read for T {
    fn by_ref(&mut self) -> &mut Self
    where
        Self: Sized 
    {
        self    
    }
}
```
```
Finished `dev` profile [unoptimized + debuginfo] target(s) in 0.05s
```
[Rust Explorer](https://www.rustexplorer.com/b#dHJhaXQgUmVhZCB7CiAgICBmbiBieV9yZWYoJm11dCBzZWxmKSAtPiAmbXV0IFNlbGYKICAgIHdoZXJlCiAgICAgICAgU2VsZjogU2l6ZWQ7Cn0KCmltcGw8VDogP1NpemVkPiBSZWFkIGZvciBUIHsKICAgIGZuIGJ5X3JlZigmbXV0IHNlbGYpIC0+ICZtdXQgU2VsZgogICAgd2hlcmUKICAgICAgICBTZWxmOiBTaXplZCAKICAgIHsKICAgICAgICBzZWxmICAgIAogICAgfQp9CgpmbiBkZW1vKHg6ICZtdXQgZHluIFJlYWQpIHsKICAgIAp9CgpmbiBtYWluKCkgewogICAgCn0=)

Rustc accepts this... Well what if we just do it for a type that isn't `Sized` directly. It definitely won't allow it then, right?

```rust
trait Read {
    fn by_ref(&mut self) -> &mut Self
    where
        Self: Sized;
}

impl Read for [i32] {
    fn by_ref(&mut self) -> &mut Self
    where
        Self: Sized 
    {
        self    
    }
}
```
```
error[E0277]: the size for values of type `[i32]` cannot be known at compilation time
  --> src/main.rs:10:9
   |
10 |         Self: Sized 
   |         ^^^^^^^^^^^ doesn't have a size known at compile-time
   |
   = help: the trait `Sized` is not implemented for `[i32]`
   = help: see issue #48214
help: add `#![feature(trivial_bounds)]` to the crate attributes to enable
   |
1  + #![feature(trivial_bounds)]
   |
```
[Rust Explorer](https://www.rustexplorer.com/b#dHJhaXQgUmVhZCB7CiAgICBmbiBieV9yZWYoJm11dCBzZWxmKSAtPiAmbXV0IFNlbGYKICAgIHdoZXJlCiAgICAgICAgU2VsZjogU2l6ZWQ7Cn0KCmltcGwgUmVhZCBmb3IgW2kzMl0gewogICAgZm4gYnlfcmVmKCZtdXQgc2VsZikgLT4gJm11dCBTZWxmCiAgICB3aGVyZQogICAgICAgIFNlbGY6IFNpemVkIAogICAgewogICAgICAgIHNlbGYgICAgCiAgICB9Cn0KCmZuIG1haW4oKSB7CiAgICAKfQ==)

See rustc isn't that weird. Its not going to allow us to do this. Makes sense right? Well add a `for<'a>` to that where bound on the impl.

```rust
trait Read {
    fn by_ref(&mut self) -> &mut Self
    where
        Self: Sized;
}

impl Read for [i32] {
    fn by_ref(&mut self) -> &mut Self
    where
        for<'a> Self: Sized 
    {
        self    
    }
}
```
```
Finished `dev` profile [unoptimized + debuginfo] target(s) in 0.05s
```
[Rust Explorer](https://www.rustexplorer.com/b#dHJhaXQgUmVhZCB7CiAgICBmbiBieV9yZWYoJm11dCBzZWxmKSAtPiAmbXV0IFNlbGYKICAgIHdoZXJlCiAgICAgICAgU2VsZjogU2l6ZWQ7Cn0KCmltcGwgUmVhZCBmb3IgW2kzMl0gewogICAgZm4gYnlfcmVmKCZtdXQgc2VsZikgLT4gJm11dCBTZWxmCiAgICB3aGVyZQogICAgICAgIGZvcjwnYT4gU2VsZjogU2l6ZWQgCiAgICB7CiAgICAgICAgc2VsZiAgICAKICAgIH0KfQoKZm4gbWFpbigpIHsKICAgIAp9)

Oh no. It allowed it... Why? Because the `for<'a>` makes rustc check the bound later than normal because ... reasons.

Wait nothing about `Sized` is special. What if we do this with `Copy`?

```rust
trait Thing {
    fn need_copy(&self)
    where
        Self: Copy;
}

impl Thing for String {
    fn need_copy(&self)
    where
        for<'a> Self: Copy
    {
        let x = *self;
        let y = x;
        let z = y;

        dbg!(x, y, z);
    }
}
```
```
Finished `dev` profile [unoptimized + debuginfo] target(s) in 0.05s
```
[Rust Explorer](https://www.rustexplorer.com/b#dHJhaXQgVGhpbmcgewogICAgZm4gbmVlZF9jb3B5KCZzZWxmKQogICAgd2hlcmUKICAgICAgICBTZWxmOiBDb3B5Owp9CgppbXBsIFRoaW5nIGZvciBTdHJpbmcgewogICAgZm4gbmVlZF9jb3B5KCZzZWxmKQogICAgd2hlcmUKICAgICAgICBmb3I8J2E+IFNlbGY6IENvcHkKICAgIHsKICAgICAgICBsZXQgeCA9ICpzZWxmOwogICAgICAgIGxldCB5ID0geDsKICAgICAgICBsZXQgeiA9IHk7CgogICAgICAgIGRiZyEoeCwgeSwgeik7CiAgICB9Cn0KCmZuIG1haW4oKSB7CiAgICAKfQ==)

That's right we made `String` implement `Copy` and we used it in the code. To prove to yourself `String` really does implement `Copy` here you can try changing the method body and rustc will treat it as it would any other code.

We have successfully made an alternative universe where `String` implements `Copy`. Something that should be impossible. Well if its an alternative universe I guess rustc doesn't care about it.
