---
date: 2024-05-23T11:49:00-07:00
tags: []
title: "Thought 0: It Begins"
---

The [nomicon](https://doc.rust-lang.org/nomicon/) warned us, but we didn't listen. There is no going back now. From here out we shall bend this infinite cosmos to our will, or at least pretend we can.
