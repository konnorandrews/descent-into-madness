---
date: 2024-05-24T11:35:00-07:00
tags: ["panic", "UB"]
title: "Thought 2: Panic Bomb"
---

Here is a panic.

```rs
panic!("help me");
```

Cute isn't it?

<!--more-->

Well what happens if we put one where Rust doesn't want us to?

```rs
struct SmallBomb;

impl Drop for SmallBomb {
    fn drop(&mut self) {
        panic!("help me");
    }
}

fn main() {
    SmallBomb;
}
```

```
thread 'main' panicked at src/main.rs:5:9:
help me
```

Hmm, I feel like something was supposed to happen. Oh I know...

```rs
struct SmallBomb;

impl Drop for SmallBomb {
    fn drop(&mut self) {
        panic!("help me");
    }
}

fn main() {
    let _x = SmallBomb;

    panic!("normal panic");
}
```

```
thread 'main' panicked at src/main.rs:12:5:
normal panic
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
thread 'main' panicked at src/main.rs:5:9:
help me
stack backtrace:
    ...
thread 'main' panicked at library/core/src/panicking.rs:164:5:
panic in a destructor during cleanup
thread caused non-unwinding panic. aborting.
```

There we go now its doing something interesting. But we all knew about this didn't we?
That a panic during a panic is an abort. Well what if we wait to panic just a little...

```rs
struct Bomb;

impl Drop for Bomb {
    fn drop(&mut self) {
        panic!("help me");
    }
}

fn main() {
    std::panic::panic_any(Bomb);
}
```

```
thread 'main' panicked at src/main.rs:10:5:
Box<dyn Any>
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
thread 'main' panicked at src/main.rs:5:9:
help me
fatal runtime error: drop of the panic payload panicked
```

Now we are talking. A fatal runtime error sounds interesting. If we have the panic message as the bomb type then the drop happens *after* the panic and starts it all over again.

I wonder what happens on older versions of `rustc`...

```
thread 'main' panicked at 'Box<Any>', /rustc/2fd73fabe469357a12c2c974c140f67e7cdd76d0/library/std/src/panic.rs:59:5
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
thread 'main' panicked at 'help me', src/main.rs:5:9
fatal runtime error: failed to initiate panic, error 5
```

Failed to initiate panic? What does that mean. Lets try miri.

```
error: Undefined Behavior: unwinding past the topmost frame of the stack
  --> /home/user/.rustup/toolchains/nightly-2021-03-23-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/std/src/r
t.rs:60:1
   |
60 | / fn lang_start<T: crate::process::Termination + 'static>(
61 | |     main: fn() -> T,
62 | |     argc: isize,
63 | |     argv: *const *const u8,
...  |
69 | |     )
70 | | }
   | |_^ unwinding past the topmost frame of the stack
   |
   = help: this indicates a bug in the program: it performed an invalid operation, and caused Undefined Behavior
   = help: see https://doc.rust-lang.org/nightly/reference/behavior-considered-undefined.html for further information
           
   = note: inside `std::rt::lang_start::<()>` at /home/user/.rustup/toolchains/nightly-2021-03-23-x86_64-unknown-linux
-gnu/lib/rustlib/src/rust/library/std/src/rt.rs:60:1
```

Well, well, well. So Rust doesn't always follow its own rules I see.

---

### Aside 1

This bug was fixed by [PR 86034](https://github.com/rust-lang/rust/pull/86034).

### Aside 2

This was supposed to be a fast little post, but it turns out the version of Rust with this bug needs xargo for miri to run. However, if you try, xargo won't build on these versions do to the ecosystem moving on and this being before cargo understood rustc versions.

```
 ► cargo miri run
I will run `"/home/user/.rustup/toolchains/nightly-2021-03-23-x86_64-unknown-linux-gnu/bin/cargo" "install" "xargo"` t
o install a recent enough xargo. Proceed? [Y/n] y
    Updating crates.io index
  Installing xargo v0.3.26
error: failed to compile `xargo v0.3.26`, intermediate artifacts can be found at `/tmp/cargo-install2x06Ev`

Caused by:
  failed to download `serde_json v1.0.117`

Caused by:
  unable to get packages from source

Caused by:
  failed to parse manifest at `/home/user/.cargo/registry/src/github.com-1ecc6299db9ec823/serde_json-1.0.117/Cargo.tom
l`

Caused by:
  feature `edition2021` is required

  consider adding `cargo-features = ["edition2021"]` to the manifest
fatal error: failed to install a recent enough xargo
```

So how did I install xargo anyways? By messing with it's Cargo.toml of course!

```diff
-rustc_version = "0.4"
-serde_json = "1.0"
+rustc_version = "=0.2.0"
+serde_json = "=1.0.0"
```

After this it installs fine. Happy miri-ing on old Rust versions!
