---
title: "Welcome to the Unknowably Infinite Cosmos"

description: "An exploration of the occult, cursed, and strange side of Rust."
---

> THE KNOWLEDGE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF UNLEASHING INDESCRIBABLE HORRORS THAT SHATTER YOUR PSYCHE AND SET YOUR MIND ADRIFT IN THE UNKNOWABLY INFINITE COSMOS.
[*The Nomicon*](https://doc.rust-lang.org/nomicon/#the-dark-arts-of-unsafe-rust*)

Welcome to the unknowably infinite cosmos. You may not have wanted to end up here but you did. We are all curios to some degree. Now there is no going back; All that is left is a decent into madness.
